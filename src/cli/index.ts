import { StateCalculationService } from '@kata/core/application/StateCalculationService.js';

const state = [
  [0, 1, 0],
  [1, 0, 1],
  [0, 0, 0],
];

const stateCalculationService = new StateCalculationService();
console.log(stateCalculationService.calculateNextGeneration(state));
