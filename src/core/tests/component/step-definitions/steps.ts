import { Given, When, Then, Before } from '@cucumber/cucumber';
import { StateCalculationService } from '../../../application/StateCalculationService.js';
import { expect } from 'chai';

Before(function (this: any) {
  this.service = new StateCalculationService();
});

Given(/^the starting position$/, function (this: any, text: string) {
  this.state = parseState(text);
});

When(/^one generation passes$/, function (this: any) {
  simulateManyTicks.call(this, 1);
});

When(/^(.*) generations pass$/, function (this: any, ticks: number) {
  simulateManyTicks.call(this, ticks);
});

Then(/^it should be$/, function (this: any, expectedState: string) {
  expect(stringifyState(this.state)).to.equal(expectedState);
});

function simulateManyTicks(this: any, ticks: number) {
  for (let i = 0; i < ticks; i++) {
    this.state = this.service.calculateNextGeneration(this.state);
  }
}

function parseState(text: string): number[][] {
  return text.split('\n').map((row) => {
    const cells: number[] = [];

    for (let i = 0; i < row.length; i++) {
      cells[i] = row.charAt(i) === '*' ? 1 : 0;
    }

    return cells;
  });
}

function stringifyState(state: number[][]): string {
  return state
    .map((row) => {
      const cells: string[] = [];

      for (let i = 0; i < row.length; i++) {
        cells[i] = row[i] === 1 ? '*' : '.';
      }

      return cells.join('');
    })
    .join('\n');
}
