Feature: Generation simulation
  Scenario: Blinker first tick
    Given the starting position
      """
      .....
      .***.
      .....
      """
    When one generation passes
    Then it should be
      """
      ..*..
      ..*..
      ..*..
      """

  Scenario: Blinker second tick
    Given the starting position
      """
      .....
      .***.
      .....
      """
    When 2 generations pass
    Then it should be
      """
      .....
      .***.
      .....
      """

  Scenario: Blinker third tick
    Given the starting position
      """
      .....
      .***.
      .....
      """
    When 3 generations pass
    Then it should be
      """
      ..*..
      ..*..
      ..*..
      """
