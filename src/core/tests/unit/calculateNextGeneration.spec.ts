import { calculateNextGeneration } from '../../domain/model/calculateNextGeneration.js';
import { State } from '../../domain/model/state.js';
import { expect } from 'chai';

/*
   1. Any live cell with fewer than two live neighbours dies
   2. Any live cell with more than three live neighbours dies
   3. Any live cell with two or three live neighbours lives on to the next generation.
   4. Any dead cell with exactly three live neighbours becomes a live cell.
 */

describe(calculateNextGeneration.name, () => {
  it('should remove any cell with fewer than two live neighbours', () => {
    const state: State = [[1, 1, 0, 0, 1, 0]];

    expect(calculateNextGeneration(state)).to.deep.equal([
      [0, 0, 0, 0, 0, 0],
    ] as State);
  });

  it('should remove living cells with more than three live neighbours', () => {
    const state: State = [
      [1, 1, 0],
      [1, 0, 0],
      [1, 1, 0],
    ];

    expect(calculateNextGeneration(state)).to.deep.equal([
      [1, 1, 0],
      [0, 0, 0],
      [1, 1, 0],
    ] as State);
  });

  it('should preserve cells with two live neighbours', () => {
    const state: State = [[1, 1, 1]];

    expect(calculateNextGeneration(state)).to.deep.equal([[0, 1, 0]] as State);
  });

  it('should revive cells with three live neighbours', () => {
    const state: State = [
      [0, 0, 0],
      [1, 1, 1],
      [0, 0, 0],
    ];

    expect(calculateNextGeneration(state)).to.deep.equal([
      [0, 1, 0],
      [0, 1, 0],
      [0, 1, 0],
    ] as State);
  });
});
