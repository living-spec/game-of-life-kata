import { State } from './state.js';

export function getNeighbors(row: number, column: number, state: State) {
  let neighbors = 0;

  for (let i = row - 1; i < row + 2; i++) {
    const rowOutOfBounds = i < 0 || i > state.length - 1;
    if (rowOutOfBounds) {
      continue;
    }

    for (let j = column - 1; j < column + 2; j++) {
      const columnOutOfBounds = j < 0 || j > state[i].length - 1;
      const self = row === i && column === j;

      if (columnOutOfBounds || self) {
        continue;
      }

      neighbors += state[i][j];
    }
  }

  return neighbors;
}
