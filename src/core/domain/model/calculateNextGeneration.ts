import { State } from './state.js';
import { getNeighbors } from './getNeighbors.js';

export function calculateNextGeneration(state: State): State {
  const newState: State = [];

  for (let i = 0; i < state.length; i++) {
    newState[i] = [];

    for (let j = 0; j < state[i].length; j++) {
      const liveNeighbors = getNeighbors(i, j, state);
      const isAlive = state[i][j] === 1;

      if (isAlive) {
        if (liveNeighbors < 2 || liveNeighbors > 3) {
          newState[i][j] = 0;
        } else if (liveNeighbors === 2) {
          newState[i][j] = 1;
        } else {
          newState[i][j] = state[i][j];
        }
      } else {
        if (liveNeighbors === 3) {
          newState[i][j] = 1;
        } else {
          newState[i][j] = state[i][j];
        }
      }
    }
  }

  return newState;
}
