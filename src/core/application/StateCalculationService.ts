import { calculateNextGeneration } from '../domain/model/calculateNextGeneration.js';
import { State } from '../domain/model/state.js';

export class StateCalculationService {
  calculateNextGeneration(state: number[][]): number[][] {
    return calculateNextGeneration(state as State);
  }
}
