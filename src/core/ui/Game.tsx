import React, { FC, PropsWithChildren, useEffect, useState } from 'react';
import { StateCalculationService } from '../application/StateCalculationService.js';
import { Component } from '@kata/ui-library/Component.js';

// FIXME: why isn't this picked up??
declare module 'react' {
  interface StyleHTMLAttributes<T> extends React.HTMLAttributes<T> {
    jsx?: boolean;
    global?: boolean;
  }
}

export default function Game() {
  const [state, setState] = useState([
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ]);

  useEffect(() => {
    const interval = setInterval(() => {
      const service = new StateCalculationService();
      setState((s) => service.calculateNextGeneration(s));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <div className="root">
      <Component />
      <div className="board">
        {state.map((row, index) => (
          <div key={index} className="row">
            {row.map((column, columnIndex) => (
              <Column key={columnIndex} state={column as any}>
                {column}
              </Column>
            ))}
          </div>
        ))}
      </div>
      <style jsx>{`
        .root {
          display: flex;
          flex-direction: column;
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          align-items: center;
          justify-content: center;
        }
        .row {
          display: flex;
          margin: 0.4rem -0.4rem;
        }
      `}</style>
    </div>
  );
}

const Column: FC<PropsWithChildren<{ state: 0 | 1 }>> = ({ state }) => (
  <div className="column">
    <style jsx>{`
      .column {
        width: 24px;
        height: 24px;
        padding: 0 0.4rem;
        background-clip: content-box;
        background-color: ${state === 1 ? 'red' : 'lightgrey'};
        border-radius: 50%;
      }
    `}</style>
  </div>
);
