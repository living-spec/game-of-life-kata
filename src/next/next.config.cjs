// @ts-check

/**
 * @type {import('next').NextConfig}
 **/
const nextConfig = {
  // typescript: {
  //   // The initial TS check goes against the tsconfig.json paths, it is not
  //   // possible to override them at this point. This will cause issues if the
  //   // targeted project uses TS Babel does not understand. We have separate
  //   // build/type-check steps anyway, so we don't need this step to be handled
  //   // by Babel anyway.
  //   ignoreBuildErrors: true,
  // },
  // eslint: {
  //   // Warning: Dangerously allow production builds to successfully complete even if
  //   // your project has ESLint errors.
  //   ignoreDuringBuilds: true,
  // },
  experimental: {
    // esmExternals: 'loose',
    externalDir: true,
  },
  // webpack: (config) => ({
  //   ...config,
  //   resolve: {
  //     ...config.resolve,
  //     // webstorm hack (paths in tsconfig-base.json)
  //     plugins: config.resolve?.plugins?.filter(removePathsPlugin),
  //   },
  // }),
}

module.exports = nextConfig;
